-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2018-01-08 17:23:06
-- 服务器版本： 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coursesel`
--

-- --------------------------------------------------------

--
-- 表的结构 `cs_sel`
--

CREATE TABLE `cs_sel` (
  `seid` int(11) NOT NULL COMMENT '选课项目id',
  `title` varchar(200) NOT NULL COMMENT '项目标题',
  `year` int(4) NOT NULL,
  `allowgroup` varchar(500) NOT NULL COMMENT '允许组合',
  `description` text NOT NULL,
  `status` tinyint(5) NOT NULL DEFAULT '0' COMMENT '状态1开启0关闭',
  `creator` varchar(50) NOT NULL COMMENT '发起人',
  `schoolid` int(11) NOT NULL COMMENT '学校id',
  `ctime` int(11) NOT NULL COMMENT '时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='选课项目表';

--
-- 转存表中的数据 `cs_sel`
--

INSERT INTO `cs_sel` (`seid`, `title`, `year`, `allowgroup`, `description`, `status`, `creator`, `schoolid`, `ctime`) VALUES
(1, '孝义四中2016届9月第一次选课', 2017, '物化生,物化政,物化历', '<p><b>说明：</b></p><p>各位请注意，本次开发的选课组合有n种。分别是物化生，物化政，物化历。请各位学生于2017年10月1日——2017年10月10日之间进行选课。逾期将关闭选课入口！</p><p align="right">谢谢合作！<br></p>', 1, 'hxb0810', 4, 1509439956),
(4, 'hkhhkhlkhl', 2017, '物化生,物化政,物化历', 'kaishila', 1, 'hxb0810', 4, 1509539046),
(5, '孝义三中2016届10月选课', 2016, '物化生,物化政,物化史,物化地,物生政,物生史,物生地,物政史,物政地,物史地', '<p><b>说明：</b><br></p><p>孝义三中2016届10月选课孝义三中2016届10月选课孝义三中2016届10月选课孝义三中2016届10月选课孝义三中2016届10月选课孝义三中2016届10月选课孝义三中2016届10月选课孝义三中2016届10月选课孝义三中2016届10月选课孝义三中2016届10月选课</p>                  ', 1, 'hxb0810', 3, 1511421397);

-- --------------------------------------------------------

--
-- 表的结构 `cs_sellog`
--

CREATE TABLE `cs_sellog` (
  `id` int(11) NOT NULL COMMENT 'id',
  `sid` int(11) NOT NULL COMMENT '学生id',
  `sname` varchar(50) NOT NULL COMMENT '学生姓名',
  `seid` int(11) NOT NULL COMMENT '选课项目id',
  `schoolid` int(11) NOT NULL COMMENT '学校id',
  `year` int(4) NOT NULL,
  `course` varchar(20) NOT NULL COMMENT '课程',
  `action` varchar(10) NOT NULL,
  `stime` int(11) NOT NULL COMMENT '选课时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='学生选课表';

--
-- 转存表中的数据 `cs_sellog`
--

INSERT INTO `cs_sellog` (`id`, `sid`, `sname`, `seid`, `schoolid`, `year`, `course`, `action`, `stime`) VALUES
(1, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509503283),
(2, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509503363),
(3, 6, '张学友', 1, 4, 2017, '物化政', '提交', 1509503375),
(4, 6, '张学友', 1, 4, 2017, '物化政', '提交', 1509504146),
(5, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509504162),
(6, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509504173),
(7, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509522125),
(8, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509522360),
(9, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509522528),
(10, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509522586),
(11, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509522672),
(12, 6, '张学友', 1, 4, 2017, '物化生', '提交', 1509522735),
(13, 6, '张学友', 1, 4, 2017, '物化生', '修改', 1509523193),
(14, 6, '张学友', 1, 4, 2017, '物化生', '修改', 1509525984),
(15, 6, '张学友', 4, 4, 2017, '物化政', '提交', 1509539361),
(16, 6, '张学友', 4, 4, 2017, '物化政', '修改', 1509539426),
(17, 6, '张学友', 1, 4, 2017, '物化政', '修改', 1509540313),
(18, 6, '张学友', 1, 4, 2017, '物化生', '修改', 1509540399),
(19, 6, '张学友', 1, 4, 2017, '物化生', '修改', 1509540444),
(20, 6, '张学友', 1, 4, 2017, '物化政', '修改', 1509541903),
(21, 6, '张学友', 1, 4, 2017, '物化生', '修改', 1509605409),
(22, 6, '张学友', 4, 4, 2017, '物化政', '提交', 1509605436),
(23, 6, '张学友', 1, 4, 2017, '物化政', '修改', 1509630684),
(24, 6, '张学友', 4, 4, 2017, '物化生', '提交', 1509630920),
(25, 6, '张学友', 1, 4, 2017, '物化生', '修改', 1509669368),
(26, 6, '张学友', 4, 4, 2017, '物化政', '修改', 1509669411),
(27, 22, '张三三', 5, 3, 2016, '物化生', '提交', 1509888750),
(28, 23, '李四四', 5, 3, 2016, '物化政', '提交', 1509888787),
(29, 22, '张三三', 5, 3, 2016, '物化史', '修改', 1509888856),
(30, 22, '张三三', 5, 3, 2016, '物化地', '修改', 1509925215),
(31, 23, '李四四', 5, 3, 2016, '物化史', '修改', 1509925416),
(32, 24, '王五五', 5, 3, 2016, '物生政', '提交', 1509925498),
(33, 23, '李四四', 5, 3, 2016, '物化地', '修改', 1511428214);

-- --------------------------------------------------------

--
-- 表的结构 `cs_selstu`
--

CREATE TABLE `cs_selstu` (
  `id` int(11) NOT NULL COMMENT 'id',
  `sid` int(11) NOT NULL COMMENT '学生id',
  `sname` varchar(50) NOT NULL COMMENT '学生姓名',
  `seid` int(11) NOT NULL COMMENT '选课项目id',
  `schoolid` int(11) NOT NULL COMMENT '学校id',
  `class` mediumint(10) NOT NULL,
  `year` int(4) NOT NULL,
  `course` varchar(20) NOT NULL COMMENT '课程',
  `stime` int(11) NOT NULL COMMENT '选课时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='学生选课表';

--
-- 转存表中的数据 `cs_selstu`
--

INSERT INTO `cs_selstu` (`id`, `sid`, `sname`, `seid`, `schoolid`, `class`, `year`, `course`, `stime`) VALUES
(2, 6, '张学友', 1, 4, 171, 2017, '物化生', 1509630684),
(20, 22, '张三三', 5, 3, 168, 2016, '物化地', 1509888750),
(19, 6, '张学友', 4, 4, 171, 2017, '物化政', 1509630920),
(21, 23, '李四四', 5, 3, 168, 2016, '物化地', 1509888787),
(22, 24, '王五五', 5, 3, 168, 2016, '物生政', 1509925498);

-- --------------------------------------------------------

--
-- 表的结构 `cs_stu`
--

CREATE TABLE `cs_stu` (
  `sid` int(11) NOT NULL COMMENT '学生id',
  `stuid` bigint(14) DEFAULT NULL COMMENT '学号/学籍号',
  `inid` int(11) DEFAULT '0' COMMENT '导入id',
  `sname` varchar(50) NOT NULL COMMENT '姓名',
  `sex` tinyint(5) DEFAULT NULL COMMENT '性别',
  `class` mediumint(10) NOT NULL COMMENT '班级',
  `year` int(6) NOT NULL COMMENT '入学年',
  `pass` varchar(50) NOT NULL DEFAULT 'e10adc3949ba59abbe56e057f20f883e' COMMENT '密码',
  `status` tinyint(5) NOT NULL DEFAULT '0' COMMENT '状态1开启0禁用',
  `schoolid` int(11) NOT NULL COMMENT '学校id',
  `ctime` int(11) NOT NULL COMMENT '时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='学生表';

--
-- 转存表中的数据 `cs_stu`
--

INSERT INTO `cs_stu` (`sid`, `stuid`, `inid`, `sname`, `sex`, `class`, `year`, `pass`, `status`, `schoolid`, `ctime`) VALUES
(1, 0, 0, '张三', 1, 168, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 0),
(2, 0, 0, '刘德华', 1, 168, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1508891850),
(5, 0, 0, '张惠妹', 0, 169, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 4, 1508898394),
(6, 201616800, 0, '张学友', 1, 171, 2017, 'e10adc3949ba59abbe56e057f20f883e', 1, 4, 1508898412),
(26, 201616802, 9, '李四四', 0, 168, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 4, 1509011193),
(27, 201616803, 9, '王五五', 1, 168, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 4, 1509011193),
(22, 201616801, 8, '张三三', 1, 168, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509011128),
(23, 201616802, 8, '李四四', 0, 168, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509011128),
(24, 201616803, 8, '王五五', 1, 168, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509011128),
(25, 201616801, 9, '张三三', 1, 168, 2016, 'e10adc3949ba59abbe56e057f20f883e', 1, 4, 1509011193),
(28, 10042010078001, 10, '许玲瑞　', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(29, 10042010078002, 10, '张鹏云　', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(30, 10042010078003, 10, '田世钊', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(31, 10042010078004, 10, '孙欣欣', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(32, 10042010078005, 10, '程应海', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(33, 10042010078006, 10, '郭良耀　', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(34, 10042010078007, 10, '张萌', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(35, 10042010078008, 10, '张腾飞　', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(36, 10042010078009, 10, '郭明磊', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(37, 10042010078010, 10, '程鹏玮', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(38, 10042010078011, 10, '张国俊', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(39, 10042010078012, 10, '乔顺法', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(40, 10042010078013, 10, '吴靖蓉', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(41, 10042010078014, 10, '郭长宸', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(42, 10042010078015, 10, '秦书磊　', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(43, 10042010078016, 10, '王冠瑛', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(44, 10042010078017, 10, '叶建伟', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(45, 10042010078018, 10, '刘晓芬', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(46, 10042010078019, 10, '郭耀华', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(47, 10042010078020, 10, '宋涛', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(48, 10042010078021, 10, '马彩霞　', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(49, 10042010078022, 10, '吕倩', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(50, 10042010078023, 10, '史清光', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(51, 10042010078024, 10, '贾云霞', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(52, 10042010078025, 10, '王慧舒', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(53, 10042010078026, 10, '田俊麟', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(54, 10042010078027, 10, '程芮', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(55, 10042010078028, 10, '任睿娣', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(56, 10042010078029, 10, '白善武', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(57, 10042010078030, 10, '樊怡秀', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(58, 10042010078031, 10, '宋彦德', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(59, 10042010078032, 10, '苗艳琴', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(60, 10042010078033, 10, '郑雅天', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(61, 10042010078034, 10, '郭二强', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(62, 10042010078035, 10, '燕红玲', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(63, 10042010078036, 10, '程裕民', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(64, 10042010078037, 10, '武彦丽', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(65, 10042010078038, 10, '王海豹', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(66, 10042010078039, 10, '李静', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(67, 10042010078040, 10, '郭德超', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(68, 10042010078041, 10, '郭彦航', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(69, 10042010078042, 10, '赵处贤', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(70, 10042010078043, 10, '张海威', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(71, 10042010078044, 10, '王文廷', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(72, 10042010078045, 10, '武莉', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(73, 10042010078046, 10, '刘二斌', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(74, 10042010078047, 10, '闫颖禄', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(75, 10042010078048, 10, '王青', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(76, 10042010078049, 10, '刘鹏晖', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(77, 10042010078050, 10, '马娟娟', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(78, 10042010078051, 10, '曹浩', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(79, 10042010078052, 10, '程琳', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(80, 10042010078053, 10, '郭兴盛', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(81, 10042010078054, 10, '刘子旗', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(82, 10042010078055, 10, '刘书杰', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(83, 10042010078056, 10, '郑彩霞', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(84, 10042010078057, 10, '任晓旺', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(85, 10042010078058, 10, '武艳红', 0, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996),
(86, 10042010078059, 10, '王乾', 1, 78, 2010, 'e10adc3949ba59abbe56e057f20f883e', 1, 3, 1509973996);

-- --------------------------------------------------------

--
-- 表的结构 `cs_stuin`
--

CREATE TABLE `cs_stuin` (
  `id` int(11) NOT NULL COMMENT '导入学生项目id',
  `title` varchar(200) NOT NULL COMMENT '项目标题',
  `iner` varchar(50) NOT NULL COMMENT '导入人',
  `schoolid` int(11) NOT NULL,
  `status` tinyint(5) DEFAULT '0',
  `intime` int(11) NOT NULL COMMENT '导入时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='导入学生项目表';

--
-- 转存表中的数据 `cs_stuin`
--

INSERT INTO `cs_stuin` (`id`, `title`, `iner`, `schoolid`, `status`, `intime`) VALUES
(8, '天天中学2016届学生', 'hxb0810', 3, 1, 1509011119),
(9, '孝义四中2016届学生名单', 'lisi', 4, 1, 1509011184),
(10, '孝义市第三中学2010测试学生名单', 'hxb0810', 3, 1, 1509973069);

-- --------------------------------------------------------

--
-- 表的结构 `cs_te`
--

CREATE TABLE `cs_te` (
  `tid` int(11) NOT NULL COMMENT '教师id',
  `schoolid` int(11) NOT NULL,
  `tname` varchar(50) NOT NULL COMMENT '登录帐号',
  `truename` varchar(50) NOT NULL COMMENT '真实姓名',
  `type` tinyint(5) NOT NULL DEFAULT '0' COMMENT '0管理员1超级',
  `pass` varchar(50) NOT NULL COMMENT '密码md5',
  `status` tinyint(5) NOT NULL DEFAULT '1' COMMENT '状态1启用，0禁用',
  `ctime` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='教师用户表';

--
-- 转存表中的数据 `cs_te`
--

INSERT INTO `cs_te` (`tid`, `schoolid`, `tname`, `truename`, `type`, `pass`, `status`, `ctime`) VALUES
(4, 4, 'lisi', '李四', 1, 'e10adc3949ba59abbe56e057f20f883e', 1, 0),
(21, 3, 'wangwu', '王五', 0, 'fcea920f7412b5da7be0cf42b8c93759', 1, 1508725820),
(19, 3, 'zhangsan', '张三', 0, 'fcea920f7412b5da7be0cf42b8c93759', 1, 1508725731),
(20, 3, 'test', '李四', 0, 'fcea920f7412b5da7be0cf42b8c93759', 1, 1508725791),
(22, 3, 'zhaoliu', '赵六', 0, 'fcea920f7412b5da7be0cf42b8c93759', 1, 1508725839),
(23, 3, 'sunqi', '孙七', 0, 'fcea920f7412b5da7be0cf42b8c93759', 1, 1508726041),
(25, 4, 'zhaosi', '赵四', 0, 'e10adc3949ba59abbe56e057f20f883e', 1, 1508827211);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cs_sel`
--
ALTER TABLE `cs_sel`
  ADD PRIMARY KEY (`seid`);

--
-- Indexes for table `cs_sellog`
--
ALTER TABLE `cs_sellog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_selstu`
--
ALTER TABLE `cs_selstu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_stu`
--
ALTER TABLE `cs_stu`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `cs_stuin`
--
ALTER TABLE `cs_stuin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_te`
--
ALTER TABLE `cs_te`
  ADD PRIMARY KEY (`tid`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `cs_sel`
--
ALTER TABLE `cs_sel`
  MODIFY `seid` int(11) NOT NULL AUTO_INCREMENT COMMENT '选课项目id', AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `cs_sellog`
--
ALTER TABLE `cs_sellog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=34;
--
-- 使用表AUTO_INCREMENT `cs_selstu`
--
ALTER TABLE `cs_selstu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=23;
--
-- 使用表AUTO_INCREMENT `cs_stu`
--
ALTER TABLE `cs_stu`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT COMMENT '学生id', AUTO_INCREMENT=87;
--
-- 使用表AUTO_INCREMENT `cs_stuin`
--
ALTER TABLE `cs_stuin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '导入学生项目id', AUTO_INCREMENT=11;
--
-- 使用表AUTO_INCREMENT `cs_te`
--
ALTER TABLE `cs_te`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT COMMENT '教师id', AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
